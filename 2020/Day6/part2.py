import time

t = time.time()

batch = []
group = []
with open('2020/Day6/input.txt') as f:
    for line in f:
        line = line.strip()
        if line:
            person = set()
            for i in range(len(line)):
                person.add(line[i])
            group.append(person)
        else:
            batch.append(group)
            group = []
    batch.append(group)

sum_count = 0
# for group in batch:
#     everyone = group[0].copy()
#     for i in range(len(group)-1):
#         person = group[i]
#         # print("DOING PERSON ", person)
#         diff = everyone.difference(person)
#         diff2 = person.difference(everyone)
#         # print("LOOK", everyone, "DIFF: ", diff, "diff2: ", diff2)
#         everyone.difference_update(diff)
#         everyone.update(diff2)
#         # print("AFTER OPERATION", everyone)
#     if len(group) != 1:
#         person = group[-1]
#         diff = everyone.difference(person)
#         diff2 = person.difference(everyone)
#         everyone.difference_update(diff)
#     # print(everyone, len(everyone))
#     sum_count = sum_count + len(everyone)

for group in batch:
    every = group[0].copy()
    for person in group:
        every.update(person)
    if not len(group) == 1:
        for person in group:
            diff = every.difference(person)
            every.difference_update(diff)
    sum_count = sum_count + len(every)

print("Answer: {}".format(sum_count))

print("Elapsed time: {:.3f}s".format(time.time() - t))