import time

t = time.time()

batch = []
group = set()
with open('2020/Day6/input.txt') as f:
    for line in f:
        line = line.strip()
        if line:
            for i in range(len(line)):
                group.add(line[i])
        else:
            batch.append(group)
            group = set()
    batch.append(group)

sum_count = 0
for group in batch:
    sum_count = sum_count + len(group)

print("Answer: {}".format(sum_count))

print("Elapsed time: {:.3f}s".format(time.time() - t))