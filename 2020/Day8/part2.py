import time

t = time.time()

program = []
with open('2020/Day8/input.txt') as f:
    for line in f:
        temp = line.strip().split(" ")
        program.append([temp[0], eval(temp[1])])

# print(program)
def check(program):
    p = program
    acc = 0
    i = 0
    visited = set()
    while True:
        if i == len(p):
            print("Answer: {}".format(acc))
            return True
        if i in visited:
            return False
        else:
            visited.add(i)
        if p[i][0] == "nop":
            i = i + 1
        elif p[i][0] == "acc":
            acc = acc + p[i][1]
            i = i + 1
        elif p[i][0] == "jmp":
            i = i + p[i][1]

i = 0
while i < len(program):
    if program[i][0] == "nop":
        program[i][0] = "jmp"
    elif program[i][0] == "jmp":
        program[i][0] = "nop"
    s = check(program)
    if s:
        break
    if program[i][0] == "nop":
        program[i][0] = "jmp"
    elif program[i][0] == "jmp":
        program[i][0] = "nop"
    i = i + 1

print("Elapsed time: {:.3f}s".format(time.time() - t))