import time

t = time.time()

area = []
with open('2020/Day3/input.txt') as f:
    for line in f:
        area.append(line.strip())

length = len(area[0])

def encounters(move):
    move_x = move[0]
    move_y = move[1]
    height = len(area) - 1
    x = 0
    y = 0
    counter = 0
    while (y < height):
        y = y + move_y
        x = (x + move_x) % length
        if area[y][x] == "#":
            counter = counter + 1
    
    return counter

slopes = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]

answer = 1
for slope in slopes:
    answer = answer * encounters(slope)

print("Answer: {}".format(answer))

print("Elapsed time: {:.3f}s".format(time.time() - t))