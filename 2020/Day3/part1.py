import time

t = time.time()

area = []
with open('2020/Day3/input.txt') as f:
    for line in f:
        area.append(line.strip())

length = len(area[0])

move_x = 3
move_y = 1

x = 0
counter = 0
for line in area[1:]:
    x = (x + move_x) % length
    if line[x] == "#":
        counter = counter + 1

print("Answer: {}".format(counter))

print("Elapsed time: {:.3f}s".format(time.time() - t))