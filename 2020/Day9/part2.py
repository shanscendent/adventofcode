import time

t = time.time()

data = []
with open('2020/Day9/input.txt') as f:
    for line in f:
        data.append(int(line.strip()))

def check(preamble, n):
    for i in range(len(preamble)):
        for j in range(i+1, len(preamble)):
            if (preamble[i] + preamble[j]) == n:
                return True
    return False

p_len = 25
preamble = [0]
preamble.extend(data[0:p_len-1])
for i in range(p_len, len(data)):
    n = data[i]
    preamble.pop(0)
    preamble.append(data[i-1])
    if not check(preamble, n):
        break

for i in range(len(data)):
    total = 0
    length = 0
    while total < n:
        if total == n:
            break
        total = total + data[i]
        length = length + 1
        i = i + 1
    if total == n:
        break

contiguous = data[i-length:i]
weakness = min(contiguous) + max(contiguous)

print("Answer: {}".format(weakness))

print("Elapsed time: {:.3f}s".format(time.time() - t))