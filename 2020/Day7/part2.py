import time

t = time.time()

rules = {}
with open('2020/Day7/input.txt') as f:
    for line in f:
        line = line.strip().split(" ")
        length = len(line)
        rule_list = {}
        if length != 7:
            try:
                counter = 0
                while True:
                    counter = counter + 4
                    num = line[counter]
                    color = "{} {}".format(line[counter+1], line[counter+2])
                    rule_list[color] = num
            except:
                rules["{} {}".format(line[0], line[1])] = rule_list
                continue
        else:
            rules["{} {}".format(line[0], line[1])] = rule_list

# for color in rules:
#     print(color, rules[color])

def count(rules, color):
    internal_counter = 0
    if not rules[color]:
        return 0
    for subcolor in rules[color]:
        number = int(rules[color][subcolor])
        internal_counter = internal_counter + number
        subcolor_count = count(rules, subcolor)
        internal_counter = internal_counter + number * subcolor_count
    return internal_counter
    
print("Answer: {}".format(count(rules, "shiny gold")))

print("Elapsed time: {:.3f}s".format(time.time() - t))