import time

t = time.time()

rules = {}
with open('2020/Day7/input.txt') as f:
    for line in f:
        line = line.strip().split(" ")
        length = len(line)
        rule_list = {}
        if length != 7:
            try:
                counter = 0
                while True:
                    counter = counter + 4
                    num = line[counter]
                    color = "{} {}".format(line[counter+1], line[counter+2])
                    rule_list[color] = num
            except:
                rules["{} {}".format(line[0], line[1])] = rule_list
                continue
        else:
            rules["{} {}".format(line[0], line[1])] = rule_list

# print(rules)

def check(rules, checked_color, color):
    if not rules[color]:
        return False
    if checked_color in rules[color].keys():
        return True
    for subcolor in rules[color]:
        if check(rules, checked_color, subcolor):
            return True
    return False

# print(check(rules, "shiny gold", "dotted black"))

counter = 0
for color in rules:
    if check(rules, "shiny gold", color):
        counter = counter + 1

print("Answer: {}".format(counter))

print("Elapsed time: {:.3f}s".format(time.time() - t))