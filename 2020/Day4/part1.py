import time

t = time.time()

batch = []
passport = {}
with open('2020/Day4/input.txt') as f:
    for line in f:
        line = line.strip()
        if line:
            line = line.split(" ")
            for field in line:
                fields = field.split(":")
                passport[fields[0]] = fields[1]
        else:
            batch.append(passport)
            passport = {}
    batch.append(passport)

fields = set(['ecl', 'pid', 'eyr', 'hcl', 'byr', 'iyr', 'hgt'])
valid = 0
for passport in batch:
    if fields.issubset(set(passport.keys())):
        valid = valid + 1

print("Answer: {}".format(valid))

print("Elapsed time: {:.3f}s".format(time.time() - t))