import time

t = time.time()

import re

batch = []
passport = {}
with open('2020/Day4/input.txt') as f:
    for line in f:
        line = line.strip()
        if line:
            line = line.split(" ")
            for field in line:
                fields = field.split(":")
                passport[fields[0]] = fields[1]
        else:
            batch.append(passport)
            passport = {}
    batch.append(passport)

fields = set(['ecl', 'pid', 'eyr', 'hcl', 'byr', 'iyr', 'hgt'])
ecl = set(['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'])
valid = 0
for passport in batch:
    if fields.issubset(set(passport.keys())):
        try:
            byr = int(passport["byr"])
            if byr < 1920 or byr > 2002:
                raise Exception
            iyr = int(passport["iyr"])
            if iyr < 2010 or iyr > 2020:
                raise Exception
            eyr = int(passport["eyr"])
            if eyr < 2020 or eyr > 2030:
                raise Exception
            hgt = passport["hgt"]
            if "cm" in hgt:
                hgt = int(hgt.split("cm")[0])
                if hgt < 150 or hgt > 193:
                    raise Exception
            else:
                hgt = int(hgt.split("in")[0])
                if hgt < 59 or hgt > 76:
                    raise Exception
            if not re.match(r'^#[0-9,a-f]{6}$', passport["hcl"]):
                raise Exception
            if passport["ecl"] not in ecl:
                raise Exception
            if not re.match(r'^[0-9]{9}$', passport["pid"]):
                raise Exception
            valid = valid + 1
        except:
            continue

print("Answer: {}".format(valid))

print("Elapsed time: {:.3f}s".format(time.time() - t))