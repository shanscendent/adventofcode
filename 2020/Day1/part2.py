import time

t = time.time()

sum_check = 2020
minus = lambda a : sum_check - a

numbers = set()
with open('2020/Day1/input.txt') as f:
    for line in f:
        numbers.add(int(line))

differences = set(map(minus, numbers))

for n in numbers:
    for m in numbers:
        if n == m:
            continue
        elif n+m in differences:
            break
    else:
        continue
    break

print("Answer: {}".format(m*n*(sum_check - m - n)))

print("Elapsed time: {:.3f}s".format(time.time() - t))