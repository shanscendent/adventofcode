import time

t = time.time()

sum_check = 2020
minus = lambda a : sum_check - a

numbers = set()
with open('2020/Day1/input.txt') as f:
    for line in f:
        numbers.add(int(line))

differences = set(map(minus, numbers))
sum_2020 = list(numbers.intersection(differences))

print("Answer: {}".format(sum_2020[0]*sum_2020[1]))

print("Elapsed time: {:.3f}s".format(time.time() - t))