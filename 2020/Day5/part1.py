import time

t = time.time()

rows = 128
cols = 8

def row(line):
    high = rows - 1
    low = 0
    partition_range = rows
    for i in range(7):
        partition_range = int(partition_range/2)
        partition = line[i]
        if partition == "F":
            high = high - partition_range
        else:
            low = low + partition_range
    return low

def col(line):
    high = cols - 1
    low = 0
    partition_range = cols
    for i in range(7, 10):
        partition_range = int(partition_range/2)
        partition = line[i]
        if partition == "L":
            high = high - partition_range
        else:
            low = low + partition_range
    return low

seats = []
with open('2020/Day5/input.txt') as f:
    for line in f:
        line = line.strip()
        seat = row(line) * 8 + col(line)
        seats.append(seat)

answer = max(seats)

print("Answer: {}".format(answer))

print("Elapsed time: {:.3f}s".format(time.time() - t))