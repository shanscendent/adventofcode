import time

t = time.time()

valid = 0
with open('2020/Day2/input.txt') as f:
    for line in f:
        temp = line.strip().split(" ")
        temp1 = temp[0].split("-")
        low = int(temp1[0])
        high = int(temp1[1])
        letter = temp[1][0]
        password = temp[2]
        times = password.count(letter)
        if times >= low and times <= high:
            valid = valid + 1

print("Answer: {}".format(valid))

print("Elapsed time: {:.3f}s".format(time.time() - t))