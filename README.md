# adventofcode

My Advent of Code solutions.

Completed:
| Day | 2020 |
| --- | --- |
| 1 | **X** |
| 2 | **X** |
| 3 | **X** |
| 4 | **X** |
| 5 | **X** |
| 6 | **X** |
| 7 | **X** |
| 8 | **X** |
| 9 | **X** |
| 10 | 
| 11 | 
| 12 | 
| 13 | 
| 14 | 
| 15 | 
| 16 | 
| 17 | 
| 18 | 
| 19 | 
| 20 | 
| 21 | 
| 22 | 
| 23 | 
| 24 | 
| 25 | 

**x** for silver, **X** for gold.
